const json = '{ "name": "Yoda", "age": 20 }';
 
try {
  const user = JSON.parse(json);
 
  if (!user.name) {
    throw new SyntaxError("'name' is required.");
  }
 
  errorCode;
 
  console.log(user.name); // Yoda
  console.log(user.age);  // 20
} catch (error) {
    // error handling
    // instaceof digunakan untuk mengecek apakah error merupakan instance dari ReferenceError.
    // Jika iya, maka blok kode di dalamnya akan dieksekusi.
    if (error instanceof SyntaxError) {
        console.log(`JSON Error: ${error.message}`);
      } else if (error instanceof ReferenceError) {
        console.log(error.message);
      } else {
        console.log(error.stack);
      }
}
 
/* output
JSON Error: errorCode is not defined
*/