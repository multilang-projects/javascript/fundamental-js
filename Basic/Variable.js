// Pada JavaScript setidaknya ada tiga cara untuk mendeklarasikan sebuah variabel, yaitu menggunakan keyword var, let, dan const.
// Namun, pada ES6 hanya ada dua cara deklarasi variabel, yaitu menggunakan let dan const. Mengapa var tidak digunakan lagi?
// Hal ini karena var memiliki masalah dalam penggunaannya, terutama dalam hal hoisting dan function scope.
// Hoisting adalah proses mengangkat deklarasi variabel atau fungsi ke atas.
// Hal ini membuat variabel dapat diakses sebelum dideklarasikan.
// Contoh:
// console.log(nama);
// var nama = 'Eko';
// Output:
// undefined
// Hal ini terjadi karena variabel nama dideklarasikan setelah console.log(nama).
// Jika menggunakan let atau const, maka akan terjadi error.
// Contoh:
// console.log(nama);
// let nama = 'Eko';
// Output:
// ReferenceError: Cannot access 'nama' before initialization

let fullName = let lastName; // Error karena let lastName adalah sebuah statement untuk deklarasi variabel. Statement tidak bisa berada di posisi expression.
let fullName = (lastName = "Skywalker"); // (lastName = "Skywalker") merupakan expression, sehingga kode ini tidak error.
let fullName = "Luke" + "Skywalker"; // "Luke" + "Skywalker" juga merupakan expression, sehingga kode ini tidak error.

const z = 100;
console.log(z);
z = 200;
console.log(z)

/* TypeError: Assignment to constant variable. */