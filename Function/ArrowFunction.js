// Arrow function adalah bentuk lain yang lebih ringkas dari function expression yang tidak memiliki nama.
// Arrow function ditandai dengan penggunaan tanda panah (=>) diantara parameter dan isi fungsi.
// Contoh:
// Arrow function tanpa parameter
const sayHello = () => {
    console.log(`Selamat pagi semuanya!`)
}
sayHello() // Output: Selamat pagi semuanya!

// Arrow function dengan satu parameter
const sayName = name => {
    console.log(`Nama saya ${name}`)
}
sayName("Harry Potter") // Output: Nama saya Harry Potter

// Arrow function dengan dua parameter
const sayNameAgain = (name, greet) => {
    console.log(`${greet}, ${name}`)
}
sayNameAgain("Harry Potter", "Halo") // Output: Halo, Harry Potter

// Ketika sebuah fungsi perlu mengembalikan nilai, kita tidak perlu lagi menuliskan return 
// (hanya bekerja untuk fungsi satu baris).
// Contoh:
const multiply = (a, b) => a * b;
console.log(multiply(3, 4));

/* output
12
 */