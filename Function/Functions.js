// Function Declaration
// Function adalah sebuah blok kode yang akan dijalankan saat kita panggil.
// Contoh:
// Function tanpa parameter
function sayHello() {
    console.log("Hello!");
}
// Function dengan parameter dan kembalian
function greeting(name, language) {
    if(language === "English") {
      return `Good Morning ${name}!`
    } else if (language === "French") {
      return `Bonjour ${name}!`;
    } else {
      return `Selamat Pagi ${name}!`;
    }
  }
  
  let greetingMessage = greeting("Harry", "French");
  console.log(greetingMessage);
  
  /* output
  Bonjour Harry!
  */

// Expression Function
// Function yang disimpan ke dalam sebuah variabel.
const greeting = function(name, language) {
    if(language === "English") {
      return "Good Morning " + name + "!";
    } else if (language === "French") {
      return "Bonjour " + name + "!";
    } else {
      return "Selamat Pagi " + name + "!";
    }
  }
  
console.log(greeting('Ron', 'English'));
  
/* output
Good Morning Ron!
*/

// kita juga bisa mengisi parameter dari object.
const user = {
    id: 24,
    displayName: 'kren',
    fullName: 'Kylo Ren',
  };
  
function introduce({displayName, fullName}) {
    console.log(`${displayName} is ${fullName}`);
}
  
introduce(user);
  
/* output
kren is Kylo Ren
*/

// Rest Parameter
// Rest parameter adalah parameter yang ditandai dengan tiga titik (...).
// Rest parameter berperan sebagai penampung nilai dari argument yang tidak terdefinisi jumlahnya.
// Contoh:
function sum(...numbers) {
    let result = 0;
    for(let number of numbers) {
      result += number;
    }
    return result;
}

console.log(sum(1, 2, 3, 4, 5));
/* output
15
*/
