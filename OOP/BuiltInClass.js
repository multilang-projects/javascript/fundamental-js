// Built-in class adalah class yang sudah disediakan oleh JavaScript.
// Built-in class yang sudah ada di JavaScript antara lain Date, Object, Array, Math, dan String.

// Built-in class Date digunakan untuk merepresentasikan tanggal dan waktu.
const date = new Date();
 
const timeInJakarta = date.toLocaleString('id-ID', {
  timeZone: 'Asia/Jakarta',
});
 
const timeInTokyo = date.toLocaleString('ja-JP', {
  timeZone: 'Asia/Tokyo',
});
// Built-in class Object digunakan untuk merepresentasikan object.
const developer = {
    name: 'Fulan',
    sayHello: () => {
      console.log(`Hello, my name is ${this.name}`);
    }
  }
   
  const engineer = new Object(developer);
  console.log(engineer);
  /* Output
  {
    name: 'Fulan',
    sayHello: [Function: sayHello]
  }
  */
   
  engineer.sayHello();
  /* Output
  Hello, my name is Fulan
  */

// Built-in class Array digunakan untuk merepresentasikan array.
const numbers = [1, 2, 3];

const numbersCopy = new Array(...numbers);
console.log(numbersCopy);

// Built-in class Math digunakan untuk melakukan operasi matematika.
const number = 3.14;

console.log(Math.round(number)); // 3
console.log(Math.floor(number)); // 3
console.log(Math.ceil(number)); // 4
console.log(Math.trunc(number)); // 3
console.log(Math.pow(number, 2)); // 9
console.log(Math.sqrt(number)); // 1.772004514666935

// Built-in class String digunakan untuk merepresentasikan string.
const string = 'JavaScript';

console.log(string.length); // 10
console.log(string.toUpperCase()); // JAVASCRIPT
console.log(string.toLowerCase()); // javascript
console.log(string.charAt(0)); // J
console.log(string.charAt((string.length) - 1));