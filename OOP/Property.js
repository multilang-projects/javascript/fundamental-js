// Properti adalah data yang dimiliki oleh sebuah object. 
// Properti dapat berupa tipe data apapun, seperti string, number, boolean, array, object, dan function. 
// Properti pada object dapat diakses menggunakan dot notation (.) atau bracket notation ([]). Contoh:

class Car {
    // Tanda # sebelum nama properti menandakan bahwa properti tersebut bersifat private
    // Private properti hanya dapat diakses di dalam class saja
    // Private properti tidak dapat diakses di luar class
    // Pengenalan private properti ini baru ada di ES2020
    #chassisNumber = null;
   
    constructor(brand, color, maxSpeed) {
      this.brand = brand;
      this.color = color;
      this.maxSpeed = maxSpeed;
      this.#chassisNumber = this.#generateChassisNumber();
   }
    // Getter    
    get chassisNumber() {
      return this.#chassisNumber;
    }
    // Setter    
    set chassisNumber(chassisNumber) {
      console.log('You are not allowed to change the chassis number');
    }
   
    // Methods
    drive() {
      console.log(`${this.brand} ${this.color} is driving`);
    }
   
    reverse() {
      console.log(`${this.brand} ${this.color} is reversing`);
    }
   
    turn(direction) {
      console.log(`${this.brand} ${this.color} is turning ${direction}`);
    }
   
    #generateChassisNumber() {
      return `${this.brand}-${Math.floor(Math.random() * 1000)}`;
    }
  }

// Membuat objek mobil dengan constructor function Car
const car = new Car('Toyota', 'Silver', 200);
console.log(car.chassisNumber);
car.#generateChassisNumber(); // Ini akan error
console.log(car.chassisNumber);
