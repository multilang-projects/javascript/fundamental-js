// Destructuring Object - ES6
// Adalah cara untuk memecah object atau array menjadi variable yang berbeda
// Contoh:
const profile = {
    firstName: "John",
    lastName: "Doe",
    age: 18
}

// const firstName = profile.firstName
// const lastName = profile.lastName
// const age = profile.age

const {firstName, lastName, age} = profile;
// Tanda kurung kurawal merepresentasikan object yang akan didestrukturisasi.
// Pastikan nama variabel yang digunakan sesuai dengan nama properti pada object.
console.log(firstName, lastName, age);
// Output: John Doe 18

// Kita juga dapat melakukan assignment ke variabel baru, seperti berikut:
const {firstName: namaDepan, lastName: namaBelakang, age: umur} = profile;
console.log(namaDepan, namaBelakang, umur);
// Output: John Doe 18

// Kita juga dapat memberikan nilai default ke variabel, seperti berikut:
const cars = {
    name: "tesla",
    model: 3,
    color: "red"
}

const {name, model, color, price = 500} = cars;
console.log(name, model, color, price);

// Kita juga dapat melakukan rest parameter, seperti berikut:
const motogp = {
    brand: "Honda",
    rider: "Marc Marquez",
    year: 2018,
} 

const {brand, ...rest} = motogp;
console.log(brand);
console.log(rest);

// Misal kita sudah mendeklarasikan variabel winner,
// kemudian kita ingin mendapatkan nilai dari properti rider pada object motogp,
// Kita dapat melakukan hal tersebut dengan cara berikut:

let winner = "Rossi";
// menginisialisasi nilai baru melalui destructuring object
({rider: winner} = motogp);
// Tanda kurung pada baris di atas diperlukan karena jika tidak ada,
// JavaScript akan menganggap bahwa kita sedang membuat block statement.
console.log(winner);