// Destructuring Array - ES6
const favorites = ["Seafood", "Salad", "Nugget", "Soup"];
 
const [firstFood, secondFood, thirdFood, fourthFood] = favorites;
 
console.log(firstFood);
console.log(secondFood);
console.log(thirdFood);
console.log(fourthFood);
 
/* output:
Seafood
Salad
Nugget
Soup
*/

// Kita tidak perlu mendeklarasikan variabel untuk setiap elemen pada array.
// Kita dapat menggunakan tanda koma kosong (,) untuk melewati elemen yang tidak ingin kita ambil.
const motogp = ["Honda", "Yamaha", "Ducati", "Suzuki", "KTM"];
const [first, , , fourth] = motogp;
console.log(first);
console.log(fourth);

// Kita juga bisa melakukan destructuring assignment pada array. 
// Namun, tidak seperti object, kita tidak perlu membungkusnya dengan tanda kurung. 
// Contohnya seperti berikut:
let myFood = "Ice Cream";
let herFood = "Noodles";
 
[myFood, herFood] = favorites;
 
console.log(myFood);
console.log(herFood);
 
/* output:
Seafood
Salad
*/

// Dengan array destructuring assignment, kita bisa menukar nilai variabel dengan mudah tanpa membuat variabel tambahan.
let a = 1;
let b = 2;
 
console.log("Sebelum swap");
console.log("Nilai a: " + a);
console.log("Nilai b: " + b);
 
[a, b] = [b, a] // menetapkan nilai a dengan nilai b dan nilai b dengan nilai a.
 
console.log("Setelah swap");
console.log("Nilai a: " + a);
console.log("Nilai b: " + b);
 
/* output
Sebelum swap
Nilai a: 1
Nilai b: 2
Setelah swap
Nilai a: 2
Nilai b: 1
*/

// Nilai default pada array destructuring assignment juga bisa kita gunakan.
const jurusan = ["Teknik Industri", "Teknik Kimia", "Teknik Informatika", "Sistem Informasi"];
const [jur1, jur2, jur3, jur4, jur5 = "Teknik Mesin"] = jurusan;
console.log(jur5);
