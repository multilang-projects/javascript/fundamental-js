// Set adalah tipe data yang menyimpan koleksi data unik, artinya tidak ada duplikasi data.
// Selain itu, data pada Set tidak berurutan dan juga tidak diindeks.
// Contoh:
const numberSet = new Set([1, 4, 6, 4, 1]);
console.log(numberSet);

// Untuk menambahkan data ke dalam Set kita bisa memanfaatkan fungsi add().
numberSet.add(5);
numberSet.add(10);
console.log(numberSet);

// Untuk menghapus data dari Set kita bisa memanfaatkan fungsi delete().
numberSet.delete(4);
console.log(numberSet);
// Ingat bahwa Set tidak memiliki urutan atau index, 
// sehingga argumen yang dimasukkan ke dalam fungsi delete adalah nilai yang ingin dihapus, bukan index-nya.
