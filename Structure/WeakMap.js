// WeakMap adalah struktur data Map yang memiliki key berupa WeakRef,
// dan value berupa value apa pun.
// WeakMap tidak memiliki property dan method yang dimiliki Map pada umumnya.
// WeakMap hanya memiliki method get(), set(), has(), dan delete().

// Berikut ini adalah beberapa hal yang membedakan antara Map dan WeakMap:
// Key dari WeakMap harus berupa object atau array. Nilai primitif tidak bisa digunakan sebagai key karena tidak mendukung garbage collection.
// WeakMap memiliki method get(), set(), has(), dan delete(). 
// Namun, WeakMap tidak termasuk kategori iterable sehingga tidak memiliki method keys(), values(), atau forEach().
// WeakMap juga tidak memiliki property size. Ini karena ukuran WeakMap dapat berubah karena proses garbage collection.

// Perbedaan Map dengan WeakMap:
// Contoh penggunaan Map:
// const visitsCountMap = new Map(); // Menyimpan daftar user

// function countUser(user) {
//   let count = visitsCountMap.get(user) || 0;
//   visitsCountMap.set(user, count + 1);
// }

// let jonas = { name: "Jonas" };
// countUser(jonas);  // Menambahkan user "Jonas"

// jonas = null;  // Data object "Jonas" dihapus

// // delay dibutuhkan untuk menunggu garbage collector bekerja
// setTimeout(function() {
//   console.log(visitsCountMap);
// }, 10000)

/* output
Map(1) { { name: 'Jonas' } => 1 }
*/

// Contoh penggunaan WeakMap:
const { inspect } = require('util');

const visitsCountMap = new WeakMap(); // Menyimpan daftar user

function countUser(user) {
  let count = visitsCountMap.get(user) || 0;
  visitsCountMap.set(user, count + 1);
}

let jonas = { name: "Jonas" };
countUser(jonas);  // Menambahkan user "Jonas"

jonas = null;  // Data object "Jonas" dihapus

// delay dibutuhkan untuk menunggu garbage collector bekerja
setTimeout(function() {
  console.log(inspect(visitsCountMap, { showHidden: true }));
}, 20000);
// Kenapa set 20000?
// Karena garbage collector bekerja setiap 15 detik sekali.
// Sebelumnya set 10000 malah tidak tampil.
/* output
  WeakMap {  }
*/