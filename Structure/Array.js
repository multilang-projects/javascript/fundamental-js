// Array merupakan tipe data yang dapat mengelompokkan lebih dari satu nilai dan menempatkannya dalam satu variabel.
// Contoh:
// let names = ["Luke", "Skywalker", "Han", "Solo"];
// console.log(names); 
// // Output: ["Luke", "Skywalker", "Han", "Solo"]

// Kita juga dapat mengakses elemen pada array dengan menggunakan indeks, seperti berikut:
let names = ["Luke", "Skywalker", "Han", "Solo"];
console.log(`Halo, nama saya ${names[0]} ${names[1]}`);

//  Kita juga dapat mengubah nilai elemen pada array dengan cara berikut:
names[0] = "Anakin";
names[2] = "Leia";
console.log(names);

// Kita juga dapat menghapus elemen pada array dengan menggunakan keyword delete seperti berikut:
delete names[1];
console.log(names);
// Output: ["Anakin", empty, "Leia", "Solo"]

// Kita juga dapat menambahkan elemen baru pada array dengan menggunakan length, seperti berikut:
names[names.length] = "Han";
console.log(names);
// Output: ["Anakin", empty, "Leia", "Solo", "Han"]

// Kita juga dapat menggunakan method push untuk menambahkan elemen baru pada array, seperti berikut:
names.push("Chewy");
console.log(names);
// Output: ["Anakin", empty, "Leia", "Solo", "Han", "Chewy"]

// Kita juga dapat menggunakan method pop untuk menghapus elemen terakhir pada array, seperti berikut:
names.pop();
console.log(names);
// Output: ["Anakin", empty, "Leia", "Solo", "Han"]

// Kita juga dapat menggunakan method unshift untuk menambahkan elemen baru pada awal array, seperti berikut:
names.unshift("Obi-Wan");
console.log(names);
// Output: ["Obi-Wan", "Anakin", empty, "Leia", "Solo", "Han"]

// Kita juga dapat menggunakan method shift untuk menghapus elemen pertama pada array, seperti berikut:
names.shift();
console.log(names);
// Output: ["Anakin", empty, "Leia", "Solo", "Han"]

// Kita juga dapat menggunakan method splice untuk menambahkan elemen baru pada indeks tertentu, seperti berikut:
names.splice(2, 0, "C-3PO");
console.log(names);
// Output: ["Anakin", empty, "C-3PO", "Leia", "Solo", "Han"]

// Method splice juga berguna untuk menghapus elemen pada indeks tertentu, seperti berikut:
names.splice(1, 2);
console.log(names);
// Output: ["Anakin", "Leia", "Solo", "Han"]

// Kita juga dapat menggunakan method slice untuk mengambil beberapa elemen pada array, seperti berikut:
let slice = names.slice(1, 3);
console.log(slice);
// Output: ["Leia", "Solo"]

// Kita juga dapat menggunakan method concat untuk menggabungkan dua array atau lebih, seperti berikut:
let names2 = ["Yoda", "Obi-Wan", "Chewy"];
let merge = names.concat(names2);
console.log(merge);
// Output: ["Anakin", "Leia", "Solo", "Han", "Yoda", "Obi-Wan", "Chewy"]

// Kita juga dapat menggunakan method sort untuk mengurutkan elemen pada array, seperti berikut:
let sort = names.sort();
console.log(sort);
// Output: ["Anakin", "Han", "Leia", "Solo"]

// Kita juga dapat menggunakan method reverse untuk membalik urutan elemen pada array, seperti berikut:
let reverse = names.reverse();
console.log(reverse);
// Output: ["Solo", "Leia", "Han", "Anakin"]

// Kita juga dapat menggunakan method indexOf untuk mencari indeks dari suatu elemen pada array, seperti berikut:
let index = names.indexOf("Han");
console.log(index);
// Output: 2

// Kita juga dapat menggunakan method join untuk menggabungkan seluruh elemen pada array menjadi sebuah string, seperti berikut:
let join = names.join();
console.log(join);
// Output: Solo,Leia,Han,Anakin

// Kita juga dapat menggunakan method forEach untuk melakukan iterasi pada seluruh elemen array, seperti berikut:
names.forEach(function (element) {
    console.log(element);
});
// Output:
// Solo
// Leia
// Han
// Anakin

// Cara yang paling cepat adalah dengan menggunakan for of dan spread operator seperti berikut:
for (const element of names) {
    console.log(element);
}
// Output:
// Solo
// Leia
// Han
// Anakin

// Kita juga dapat menggunakan spread operator untuk menggabungkan seluruh elemen pada array menjadi sebuah array baru, seperti berikut:
console.log(...names);
// Output: Solo Leia Han Anakin

// Kita juga dapat menggunakan method map untuk melakukan iterasi pada seluruh elemen array dan mengembalikan array baru, seperti berikut:
let map = names.map(function (element) {
    return element;
});
console.log(map);
// Output: ["Solo", "Leia", "Han", "Anakin"]

// Kita juga dapat menggunakan method filter untuk melakukan iterasi pada seluruh elemen array dan mengembalikan array baru yang telah difilter, seperti berikut:
let filter = names.filter(function (element) {
    return element.length > 3;
}
);
console.log(filter);
// Output: ["Solo", "Leia", "Anakin"]

// Kita juga dapat menggunakan method reduce untuk melakukan iterasi pada seluruh elemen array dan mengembalikan nilai akhir, seperti berikut:
let reduce = names.reduce(function (accumulator, currentValue) {
    return accumulator + currentValue;
});
console.log(reduce);
// Output: SoloLeiaAnakinHan
