// Object adalah tipe data yang berisi kumpulan data dan fungsi yang terorganisir.
// Contoh:
// let object = {key1: "value1", key2: "value2", key3: "value3"}
// Key harus berupa string dan dituliskan sebelum titik dua (:), lalu diikuti dengan value-nya. 
// Meskipun key merupakan string, kita tidak perlu menuliskan tanda petik kecuali ada karakter khusus seperti spasi.

const user = {
    firstName: "Luke",
    lastName: "Skywalker",
    "Nama Lengkap": "Luke Skywalker",
    age: 19,
    isJedi: true,
};
console.log(`Halo, nama saya ${user.firstName} ${user.lastName}`);
// Tanda koma pada properti terakhir bersifat opsional. 
// Namun, jika tanda koma tersebut ditulis akan lebih memudahkan ketika kita ingin memindah, mengubah, atau menghapus properti.

// Untuk mengakses key yang memiliki spasi atau karakter khusus lainnya maka kita perlu menggunakan bracket seperti berikut.
console.log(`Halo, nama saya ${user["Nama Lengkap"]}`);

// Kita bisa mengubah nilai properti dengan cara berikut:
user.firstName = "Anakin";
user["lastName"] = "Silaing";
console.log(`Halo, nama saya ${user.firstName} ${user.lastName}`);

// Kita juga dapat menghapus property pada object menggunakan keyword delete seperti berikut:
delete user.age;
console.log(user);