// Map adalah tipe data yang menyimpan koleksi data dengan format key-value layaknya Object. 
// Yang membedakan adalah Map memperbolehkan key dengan tipe data apa pun, 
// dibandingkan Object yang hanya mengizinkan key bertipe String atau Symbol.

const capital = new Map([
    ["Jakarta", "Indonesia"],
    ["London", "England"],
    ["Tokyo", "Japan"]
]);
  
console.log(capital.size);
// method get() digunakan untuk mendapatkan nilai dari key yang diberikan.
console.log(capital.get("London"));
// method set() digunakan untuk menambah pasangan key dan value baru.
capital.set("New Delhi", "India");
// method size digunakan untuk mendapatkan jumlah pasangan key dan value pada Map.
console.log(capital.size);
console.log(capital.get("New Delhi"));
  
  /* output
  3
  England
  4
  India
   */
